//Imports
const db = require('../MOCK_DATA.json')

//Récupérer tout les utilisateurs
const getAllUser = (request, response) => {
    return response.status(200).json(db);
}

//Récupérer un utilisateur en fonction de son ID
const getUserById = (request, response) => {
    const id = parseInt(request.params.id);
    var item = findId(db,id);
    return response.status(200).json(item);
}

//Fonction permettant de chercher en fonction d'un ID
function findId(data, idToLookFor) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].id == idToLookFor) {
            return(data[i]);
        }
    }
    return(null)
}

//Export des modules
module.exports = {
    getUserById,
    getAllUser,
    findId
}