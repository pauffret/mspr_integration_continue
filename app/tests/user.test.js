const { test, expect } = require('@jest/globals');
const user = require('../routes/user');
const db = require('../MOCK_DATA.json');

//Test fonction findId
test('Should not be null', () => {
    expect(user.findId(db,1).first_name).not.toBe(null);
});
test('Should return first name', () => {
    expect(user.findId(db,1).first_name).toBe("testRitchie");
});
test('Should return last name', () => {
    expect(user.findId(db,1).last_name).toBe("Dane");
});
test('Should return email', () => {
    expect(user.findId(db,1).email).toBe("rdane0@angelfire.com");
});
test('Should return the id 1', () => {
    expect(user.findId(db,1).id).toBe(1);
});
test('Should return the id 1', () => {
    expect(user.findId(db,26)).toBe(null);
});

