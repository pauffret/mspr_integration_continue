const { test, expect } = require('@jest/globals');
const app = require('../server');
const supertest = require("supertest");

test("GET /users", async () => {
    await supertest(app).get("/users")
      .expect(200)
      .then((response) => {
        // Check type and length
        expect(response.body.length).toEqual(25);
      });
  });

test("GET /user/1", async () => {
    await supertest(app).get("/user/1")
      .expect(200)
      .then((response) => {
        // Check type and length
        expect(response.body.id).toEqual(1);
      });
  });