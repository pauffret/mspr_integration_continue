//Import dependances
const express = require('express')
const app = express()

//Monitoring
const config = {
  title: 'MSPR Intégration',
  path: '/status',
  spans: [{
    interval: 1,
    retention: 60
  }, {
    interval: 5,
    retention: 60
  }, {
    interval: 15,
    retention: 60
  }],
  chartVisibility: {
    cpu: true,
    mem: true,
    load: true,
    responseTime: true,
    rps: true,
    statusCodes: true
  },
  healthChecks: [
    {
      protocol: 'http',
      host: 'localhost',
      path: '/users',
      port: '8080'
    },
    {
      protocol: 'http',
      host: 'localhost',
      path: '/user/:id',
      port: '8080'
    }
  ],
  ignoreStartsWith: '/admin'
}

// Monitoring
app.use(require('express-status-monitor')(config));

//Routes
const user = require('./routes/user')

//Routes User
app.get('/user/:id', user.getUserById)
app.get('/users', user.getAllUser)


module.exports = app;