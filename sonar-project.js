const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner({
    serverUrl: 'http://51.15.240.31:9000/',
    options: {
        'sonar.projectKey': 'mspr-integration',
        'sonar.sources': '.',
        'sonar.tests': '.',
        'sonar.inclusions': 'app/**',
        'sonar.test.inclusions':  'app/tests/*.test.js',
        'sonar.javascript.lcov.reportPaths':  'coverage/lcov.info',
        'sonar.testExecutionReportPaths':  'coverage/test-reporter.xml'
    },
}, () => { });